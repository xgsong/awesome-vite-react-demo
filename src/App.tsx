import React, { createContext, useContext, useRef, useState } from "react";
import "./App.css";
import InputField from "./components/InputField";
import TodoList from "./components/TodoList";
import { Todo } from "./model";

interface AppContext {
  todos: Todo[];
  setTodos: React.Dispatch<React.SetStateAction<Todo[]>>;
}

export const ctx = createContext<AppContext>({} as AppContext);

function App() {
  const [todos, setTodos] = useState<Todo[]>([] as Todo[]);

  return (
    <div className="App">
      <ctx.Provider value={{ todos, setTodos }}>
        <span className="heading">Billy Song's Taskify</span>
        <InputField />
        <TodoList todos={todos} />
      </ctx.Provider>
    </div>
  );
}

export default App;
