import React, { useContext, useRef } from "react";
import { ctx } from "../App";
import "./style.css";

const InputField: React.FC = () => {
  const { todos, setTodos } = useContext(ctx);
  const inputRef = useRef<HTMLInputElement>(null);

  const handleAdd = (e: React.FormEvent) => {
    e.preventDefault();
    if (inputRef.current) {
      setTodos([
        ...todos,
        {
          id: Date.now(),
          todo: inputRef.current.value,
          isDone: false,
        },
      ]);
      inputRef.current.value = "";
    }
  };

  return (
    <form
      className="input"
      onSubmit={(e) => {
        handleAdd(e);
        inputRef.current?.blur();
      }}
    >
      <input
        className="input-box"
        type="input"
        placeholder="Enter a task."
        ref={inputRef}
      />
      <button className="input-submit" type="submit">
        Go
      </button>
    </form>
  );
};

export default InputField;
