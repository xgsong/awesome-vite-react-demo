import React, { useContext, useState } from "react";
import { Todo } from "../model";
import { AiFillDelete, AiFillEdit } from "react-icons/ai";
import { MdDone } from "react-icons/md";
import { ctx } from "../App";

interface Props {
  todo: Todo;
}

const TodoItem: React.FC<Props> = ({ todo }) => {
  const { todos, setTodos } = useContext(ctx);

  const [edit, setEdit] = useState(false);
  const [editTodo, setEditTodo] = useState(todo.todo);

  const handleEdit = (e: React.FormEvent, todoId: number, todo: string) => {
    e.preventDefault();
    setTodos(
      todos.map((item) => (item.id === todoId ? { ...item, todo: todo } : item))
    );
  };

  const handleToggle = (todoId: number) => {
    setTodos(
      todos.map((item) =>
        item.id === todoId ? { ...item, isDone: !item.isDone } : item
      )
    );
  };

  const handleDelete = (todoId: number) => {
    setTodos(todos.filter((item) => item.id !== todoId));
  };

  return (
    <form
      className="todo-item"
      onSubmit={(e) => {
        handleEdit(e, todo.id, editTodo);
        setEdit(!edit);
      }}
    >
      {edit ? (
        <input
          className="todo-item-text"
          value={editTodo}
          onChange={(e) => setEditTodo(e.target.value)}
        />
      ) : todo.isDone ? (
        <s className="todo-item-text">{todo.todo}</s>
      ) : (
        <span className="todo-item-text">{todo.todo}</span>
      )}

      <div>
        <span
          className="icon"
          onClick={(e) => {
            if (!edit && !todo.isDone) {
              setEdit(!edit);
            }
          }}
        >
          <AiFillEdit />
        </span>
        <span className="icon" onClick={(e) => handleDelete(todo.id)}>
          <AiFillDelete />
        </span>
        <span className="icon" onClick={(e) => handleToggle(todo.id)}>
          <MdDone />
        </span>
      </div>
    </form>
  );
};

export default TodoItem;
